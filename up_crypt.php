<?php

class up_crypt
{
    public static function uuid4()
	{
		// https://stackoverflow.com/a/15875555
		$data = openssl_random_pseudo_bytes(16);
		assert(strlen($data) == 16);
		$data[6] = chr(ord($data[6]) & 0x0f | 0x40);
		$data[8] = chr(ord($data[8]) & 0x3f | 0x80);
		return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
	}

	public static function isuuid4(string $UUIDv4): bool
	{
		$UUIDv4Format = '/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i';
		if (preg_match($UUIDv4Format, $UUIDv4)) {
			return true;
		}
		return false;
	}
}