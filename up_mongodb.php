<?php

class up_mongodb
{

    public static $host = null;
    public static $username = null;
    public static $password = null;
    public static $port = null;
    public static $collection = null;
    public static $collectionName = null;
    public static $database = null;

    private static function connection()
    {
        if (self::$host == null || self::$port == null) {
            trigger_error('invalid MongoDB credentials');
        }

        try {
            if (self::$username == null && self::$password == null) {
                $client = new MongoDB\Client("mongodb://" . self::$host . ":" . self::$port);
            } else {
                $client = new MongoDB\Client("mongodb://" . self::$username . ":" . self::$password . "@" . self::$host . ":" . self::$port . "/" . self::$database);
            }
        } catch (Exception $e) {
            trigger_error('Caught exception: ' . $e->getMessage());
        }
        if (self::$database == null || self::$collectionName == null) {
            trigger_error('invalid database or collectionName');
        } else {
            self::$collection = $client->{self::$database}->{self::$collectionName};
        }
    }

    public static function insertOne($array)
    {
        if (self::$collection == null) {
            self::connection();
        }

        $array = (array) $array;
        $result  = self::$collection->insertOne($array);
        return $result->getInsertedId();
    }

    public static function findOne($mongo_id)
    {
        if (self::$collection == null) {
            self::connection();
        }

        if ($mongo_id != "1") {
            $data = self::$collection->findOne(
                array(
                    '_id' => new MongoDB\BSON\ObjectId($mongo_id)
                )
            );
        } else {
            $data = [];
        }
        return $data;
    }

    public static function findKeysValue($keyValues)
    {
        if (self::$collection == null) {
            self::connection();
        }

        $condition = [];

        foreach ($keyValues as $key => $value) {

            if (is_numeric($value))
                array_push($condition, [$key => $value]);
            else if (is_string($value)) {
                array_push($condition, [$key => ['$regex' => $value,  '$options' => '-i']]);
            }
        }
        $cursor = self::$collection->find(array('$and' => $condition));
        $data = iterator_to_array($cursor);
        return $data;
    }
}
