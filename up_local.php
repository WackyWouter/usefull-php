<?php

class h_local
{
	public static $path = false;
	public static $default = null;
	public static $available = null;
	private static $translate_array = [];

	public static function words($domain, $words, $alt_language = false)
	{
		if (strlen(trim($words)) == 0) return $words;

		global $security;

		$language = (isset($security->language)) ? $security->language : false;
		if ($language === false) {
			$language = h_local::preferedLanguage();
		}

		if ($alt_language !== false) {
			$language = $alt_language;
		}

		if (!isset(self::$translate_array[$language][$domain])) {
			$path = self::$path;

			if (self::$path === false) {
				$path = BASE_PATH;
			}

			if (file_exists($path . 'locale/' . $language . '/' . $domain . '.php')) {
				include_once $path . 'locale/' . $language . '/' . $domain . '.php';
				self::$translate_array[$language][$domain] = $text;
			} else if (file_exists($path . 'locale/en-US/' . $domain . '.php')) {
				include_once $path . 'locale/en-US/' . $domain . '.php';
				$translate_array[$language][$domain] = $text;
			} else if (file_exists($path . 'locale/en-US/' . $domain . '.php')) {
				include_once $path . 'locale/en-US/' . $domain . '.php';
				self::$translate_array[$language][$domain] = $text;
			}
		}

		$debugTranslations = false;

		if (isset(self::$translate_array[$language][$domain][$words])) {
			if ($debugTranslations) {
				return '<span title="' . $language . ' - ' . $domain . '" class=\'translationFound\'>' . self::$translate_array[$language][$domain][$words] . '</span>';
			}
			return self::$translate_array[$language][$domain][$words];
		}

		if ($debugTranslations) {
			return '<span title="' . $language . ' - ' . $domain . '" class=\'translationNotFound\'>' . $words . '</span>';
		}

		return $words;
	}

	protected static function preferedLanguage()
	{
		if (self::$default == null) {
			self::$default = (defined('LANGUAGE')) ? LANGUAGE : null;;
		}
		if (self::$available == null) {
			self::$available = (defined('LANGUAGES')) ? LANGUAGES : null;
		}

		$http_accept_language = (isset($_SERVER["HTTP_ACCEPT_LANGUAGE"])) ? $_SERVER["HTTP_ACCEPT_LANGUAGE"] : self::$default;
		$available_languages = '';
		$langs = array();

		if (self::$available) {
			$available_languages = explode(',', self::$available);
		} else {
			$available_languages = explode(',', $http_accept_language);
			$available_languages = array_flip($available_languages);
		}

		preg_match_all('~([\w-]+)(?:[^,\d]+([\d.]+))?~', strtolower($http_accept_language), $matches, PREG_SET_ORDER);

		foreach ($matches as $match) {
			list($a, $b) = explode('-', $match[1]) + array('', '');
			$value = isset($match[2]) ? (float) $match[2] : 1.0;

			if (isset($available_languages[$match[1]])) {
				$langs[$match[1]] = $value;
				continue;
			}

			if (isset($available_languages[$a])) {
				$langs[$a] = $value - 0.1;
			}
		}

		if (count($langs) == 0) {
			return self::$default;
		}

		arsort($langs);

		reset($langs);

		return key($langs);
	}

	public static function date($time)
	{
		if ($time == null) {
			return '';
		}

		return strftime('%c', $time);
	}

	public static function normalize($string)
	{
		$table = array(
			'Š' => 'S', 'š' => 's', 'Ð' => 'Dj', 'Ž' => 'Z', 'ž' => 'z', 'C' => 'C', 'c' => 'c', 'C' => 'C', 'c' => 'c',
			'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E',
			'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O',
			'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss',
			'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e',
			'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o',
			'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'ý' => 'y', 'þ' => 'b',
			'ÿ' => 'y', 'R' => 'R', 'r' => 'r',
		);

		return strtr($string, $table);
	}
}
