<?php
require __DIR__ . '/../configuration.php';

class up_database
{
	private static $mysqlis = array();
	private static $ignoreErrors = false;

	public static $host = null;
	public static $username = null;
	public static $passwd = null;
	public static $dbname = null;
	public static $port = null;
	public static $socket = null;

	//Please, do NOT user $ignoreErrors. This is intended for situations where no other method is viable!
	//We do want to go back to a single database connection, but we need it now for translations (and menu)

	public static function prepare($query, $ignoreErrors = false)
	{
		if (self::$host == null) {
			self::$host = DB_HOST;
			self::$username = DB_UID;
			self::$passwd = DB_PWD;
			self::$dbname = DB_NAME;
		}

		$stmt = false;
		$index = 0;
		while ($stmt === false) {
			if (!isset(self::$mysqlis[$index])) {
				self::$mysqlis[$index] = new mysqli(self::$host, self::$username, self::$passwd, self::$dbname, self::$port, self::$socket);

				if (self::$mysqlis[$index]->connect_error) {
					die('Unable to open the database');
				}

				self::$mysqlis[$index]->set_charset("utf8mb4");
			}
			$stmt = self::$mysqlis[$index]->prepare($query);

			if (self::$mysqlis[$index]->errno > 0 && self::$mysqlis[$index]->errno != 2014) {
				trigger_error(self::$mysqlis[$index]->error);
				break;
			}

			if ($stmt === false) {
				$index++;
			}
		}

		if ($ignoreErrors) {
			self::$ignoreErrors = $ignoreErrors;
		}

		if (!self::$ignoreErrors && count(self::$mysqlis) > 2) {
			trigger_error('To many database connections: ' . count(self::$mysqlis) . PHP_EOL . $query);
		}

		if ($stmt) {
			$stmt->store_result();
		}

		return $stmt;
	}

	public static function affectedRows(): Int
	{
		if (isset(self::$mysqlis[0])) {
			return self::$mysqlis[0]->affected_rows;
		}
		return 0;
	}

	public static function logError($stmt)
	{
		if ($stmt->error == null) {
			return;
		}

		ob_start();
		debug_print_backtrace();
		error_log(ob_get_clean());
		trigger_error($stmt->error);
    }

    public static function serverError($stmt)
	{
		if($stmt->error == null){
            return;
        }
        trigger_error($stmt->error);
        header("Server error", true, 500);
        exit;
	}

	public static function reset()
	{
		foreach (self::$mysqlis as $mysqli) {
			$mysqli->close();
		}
		self::$mysqlis = array();
	}

	public static function import($file)
	{
		if (!is_file($file)) {
			trigger_error('No file' . $file);
			return;
		}

		$sql = file_get_contents($file);

		echo 'multi_query' . PHP_EOL;
		$mysqli = new mysqli(self::$host, self::$username, self::$passwd, self::$dbname);
		if ($mysqli->multi_query($sql)) {
			do {
				/* store first result set */
				if ($result = $mysqli->store_result()) {
					while ($row = $result->fetch_row()) {
						printf("%s\n" . PHP_EOL, $row[0]);
					}
					$result->free();
				}
				/* print divider */
				if ($mysqli->more_results()) {
					printf("-----------------\n" . PHP_EOL);
				}
			} while ($mysqli->next_result());
		} else {
			echo 'error';
		}
		mysqli_close($mysqli);


		// echo 'line by line' . PHP_EOL;
		// $mysqli = new mysqli(self::$host, self::$username, self::$passwd, self::$dbname);
		// $op_data = '';
		// $lines = file($file);
		// foreach ($lines as $line) {
		// 	if (substr($line, 0, 2) == '--' || $line == '') //This IF Remove Comment Inside SQL FILE
		// 	{
		// 		continue;
		// 	}
		// 	$op_data .= $line;
		// 	if (substr(trim($line), -1, 1) == ';') //Breack Line Upto ';' NEW QUERY
		// 	{
		// 		$mysqli->query($op_data);
		// 		$op_data = '';
		// 	}
		// }
		// mysqli_close($mysqli);

		echo 'done' . PHP_EOL;
	}
}
