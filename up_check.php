<?php

class up_check
{

	public static function passwordStrength($password)
	{
		$checkCounter = 0;
		if (strlen($password) >= 8) {
			$checkCounter++;
		}
		for ($i = 0; $i < strlen($password); $i++) {
			$ascii = ord(substr($password, $i, 1));
			if ($ascii >= 65 && $ascii <= 90) {
				$checkCounter++;
				break;
			}
		}
		for ($i = 0; $i < strlen($password); $i++) {
			$ascii = ord(substr($password, $i, 1));
			if ($ascii >= 48 && $ascii <= 57) {
				$checkCounter++;
				break;
			}
		}
		if ($checkCounter == 3) {
			return true;
		}

		return false;
    }

    public static function checkPassword($user_id, $password){
        $passwordDB = null;
        
        if ($stmt = up_database::prepare('SELECT 
                                                AES_DECRYPT(password, UNHEX(SHA2(user_uuid, 512))) 
                                            FROM 
                                                users 
                                            WHERE 
                                                user_uuid = ?')) {
            $stmt->bind_param('s', $user_id);
            $stmt->execute();
            $stmt->bind_result($passwordDB);
            $stmt->fetch();
            
            
            if($password === $passwordDB){
                $stmt->close();
                return true;
            }                     
        }
        $stmt->close();
        return false;
    }

    public static function checkPasswordMatch($newPassword, $confirmPassword){
        if($newPassword === $confirmPassword){
            return true;
        }
        return false;
    }
    
    // public static function check($cli = false, $testData = null): void
    // {
    //     if ($cli == false) {
    //         // - only allow GET, POST, DELETE requests
    //         if (!in_array($_SERVER['REQUEST_METHOD'], ['GET', 'POST', 'DELETE'])) {
    //             self::generateError('405 Method Not Allowed');
    //         }

    //         // - application key in header (uuidv4), do we expect it?
    //         if (!isset($_SERVER['HTTP_X_APPLICATION_KEY']) || !up_crypt::isuuid4($_SERVER['HTTP_X_APPLICATION_KEY']) || !self::appUuidAllowed()) {
    //             self::generateError('400 Bad Request (HTTP_X_APPLICATION_KEY)');
    //         }

    //         # Get JSON as a string
    //         $json_str = file_get_contents('php://input');

    //         # Get as an object
    //         $items = json_decode($json_str);

    //         if ($items == null && json_last_error() !== JSON_ERROR_NONE) {
    //             self::generateError('417 Invalid JSON');
    //         }

    //         if (!array_key_exists('user_uuid', $items)) {
    //             request::generateError('404 No user_uuid key found', false);
    //         }

    //         if (!h_crypt::isuuid4($items->user_uuid)) {
    //             request::generateError('404 Invalid user UUID', false);
    //         }
    //         self::$body = $items;
    //     } else if ($cli) {
    //         self::$body = (object) $testData;
    //     }
    // }


}
